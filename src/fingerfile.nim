import 
    jsony,
    bytesequtils,
    std/[algorithm, sequtils, tables,
    strutils, os, monotimes, times]

export jsony

type
    FileType = object 
        `type`, extension, mime: string
        offset: uint8
        signature: seq[string]
    FileTypes = seq[FileType]
    
    FileInfo* = object
        extensions, mimes, types: seq[string]
    FileInformation* = object
        `type`, extension, mime: string
    Fingerprint = tuple[signature:string,offset: uint8]
    FileMap* = Table[Fingerprint, seq[FileInformation]]

proc `$`*(f: FileInformation): string =
    return f.toJson()

proc `$`*(f: seq[FileInformation]): string =
    return f.toJson()

proc readJson(): FileMap =
    var parsed = staticRead("data.json").fromJson(FileTypes)
    for i in 0..<parsed.len:
        for h in 0..<parsed[i].signature.len:
           parsed[i].signature[h] = parsed[i].signature[h].replace(" ", "").parseHexStr
    var fm = initTable[Fingerprint, seq[FileInformation]]()
    for p in parsed:
        for s in p.signature:
            if not fm.hasKey((s, p.offset)):
                fm[(s, p.offset)] = newSeq[FileInformation]()
            fm[(s, p.offset)].add FileInformation(`type`: p.`type`, extension: p.extension, mime: p.mime)
    return fm

proc parseInfo(): FileInfo =
    func ds(s: var seq[string]) =
        s = s.deduplicate.sorted[1..^1]
    let ft = staticRead("data.json").fromJson(FileTypes)
    result = FileInfo()
    for p in 0..<ft.len:
        result.mimes.add ft[p].mime
        result.extensions.add ft[p].extension
        result.types.add ft[p].`type`
    result.mimes.ds
    result.extensions.ds
    result.types.ds

const 
    info = readJson()
    support = parseInfo()

proc readFingerprint(file: string, buffSize: int16 = 128): string {.inline.} =
    var buff = newSeq[byte](buffSize)
    let fnb =  open(file, fmRead, buffSize)
    discard fnb.readBytes(buff,0,buffSize)
    fnb.close()
    return buff.toStrBuf

proc getInfoList*(file: string): seq[FileInformation] =
    if file.fileExists:
        let fstr = readFingerprint(file)
        for i in info.keys():
            let 
                filesig = fstr[i.offset..^1]
                sig = i.signature
            if sig.len <= filesig.len and sig == filesig[0..<sig.len]:
                return info[i]
    else:
        raise newException(IOError, "Failed to load file")

proc getInfoMatch*(file: string, matchExtension: bool = false) : FileInformation =
    let listing = getInfoList(file)
    for l in listing:
        if file.endsWith(l.extension):
            return l
    if not matchExtension:
        if listing.len == 1:
            return listing[0]
        else:
            raise newException(ValueError, "Failed to identify file exclusively")

proc supportedTypes*(): seq[string] =
    # Returns a list of supported file types 
    return support.types

proc supportedExtensions*(): seq[string] =
    # Returns a list of supported file extensions 
    return support.extensions

proc supportedMimes*(): seq[string] =
    # Returns a list of supported file MIME types 
    return support.mimes

when isMainModule:
    let 
        startBench = getMonoTime()
        cmdp = commandLineParams()
    for i in cmdp:
        try:
            echo "$1: $2" % [absolutePath(i), $getInfoMatch(i)]
        except:
            echo """$1: {"type":$2,"extension":$2,"mime":$2}""" % [i, "null"]
    stderr.write "Elapsed ($1 files): $2 \n" % [$(cmdp.len), $(getMonoTime() - startBench)]
