# Package

version       = "0.3"
author        = "juujaa"
description   = "Recognize filetype automatically"
license       = "MIT"
srcDir        = "src"
installFiles  = @["data.json", "fingerfile.nim"]



# Dependencies

requires "jsony, bytesequtils"
